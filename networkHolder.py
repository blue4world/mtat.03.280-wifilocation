from sampleHolder import *
import numpy as np
import scipy.stats as stats

class NetworkHolder:
    def __init__(self,ssid,mac):
        self.topSignal = 100
        self.ssid = ssid.lower()
        self.mac = mac.lower()
        self.samples = list()
        self.stable = False
        self.averageSignal = 0
        self.mean = 0
        self.std = 0
        self.var = 0
        self.tempCorrelation = 0
        self.sunCorrelation = 0
        self.motionCorrelation = 0


    def getSamplesForSpecificHour(self,hourInput,multiplier = 6,sort = True):
        specificSamples = []
        for sample in self.samples:
            date = datetime.datetime.fromtimestamp(float(sample.timestamp))
            hour = date.strftime('%H')
            hour = int(hour)
            if hour <= hourInput*multiplier:
                specificSamples.append(sample.level)
        if sort == True:
            specificSamples = NetworkHolder.sortSignal(specificSamples)
        return specificSamples


    def groupBySamples(self,numOfSamplesPerBucket):
        newSamples = []
        i = 0
        temp = [ ]
        for sample in self.samples:
            temp.append(abs(self.topSignal+int(sample.level)))
            i += 1
            if i == numOfSamplesPerBucket:
                mean = np.mean(temp)
                temp = [ ]
                i = 0
                newSamples.append(mean)
        return newSamples

    def addSample(self,time,signal_level,signal_quality,sun = 0, motion = 0,temperature = 0):
        self.samples.append(SampleHolder(time,signal_level,signal_quality,sun,motion,temperature))

    def computeAverageSignal(self):
        sum = 0
        for sample in self.samples:
            sum = sum + abs(self.topSignal+int(sample.level))
        self.averageSignal = float(sum) / float(len(self.samples))

    def getSortedSignal(self):
        signal = list()
        for sample in self.samples:
            signal.append(abs(self.topSignal+int(sample.level)))
        signal = sorted(signal)
        return signal

    @staticmethod
    def sortSignal(signalInput):
        signal = []
        for sample in signalInput:
            signal.append(abs(100+int(sample)))
        signal = sorted(signal)
        return signal

    def getSignal(self):
        signal = list()
        for sample in self.samples:
            signal.append(abs(self.topSignal+int(sample.level)))
        return signal

    def getTemp(self):
        temp = list()
        for sample in self.samples:
            num = float(sample.temperature)
            temp.append(num)
        return temp

    def getSun(self):
        sun = list()
        for sample in self.samples:
            num = float(sample.sun)
            sun.append(num)
        return sun

    def getMotion(self):
        motion = list()
        for sample in self.samples:
            num = float(sample.motion)
            motion.append(num)
        return motion

    def findCorelation(self):
        signal = self.getSignal()
        #print np.correlate(signal1,signal2)
        self.tempCorrelation = (stats.pearsonr(signal, self.getTemp()))
        self.motionCorrelation = (stats.pearsonr(signal, self.getMotion()))
        self.sunCorrelation = (stats.pearsonr(signal, self.getSun()))


    def computeStatistics(self):
        signal = self.getSortedSignal()
        self.mean = np.mean(signal)
        self.std = np.std(signal)
        self.var = np.var(signal)


