import csv
import operator
import matplotlib.pyplot as plt
from networkHolder import *
import pylab as pl
from scipy import stats
import sys
from sklearn.preprocessing import normalize

class WifiStats:
    listOfInstances = list()
    #colors = ['red','green','blue','pink','yellow']
    #colors = ['black','red','green','blue']
    colors = ['red','green','blue']

    def __init__(self,filename,extended,stableAps = 0.9):
        self.listOfInstances.append(self)
        self.filename = filename
        self.networks = list()
        self.numOfSamples = 0
        self.excludedSSIDs = ["campuswifi", "ut-public"]
        self.std = 0
        self.mean = 0
        self.var = 0
        self.meanSignal = list()

        # run part
        self.loadFromFile(extended)
        self.determineStableAp(stableAps)
        self.calculateAverageSignal()
        self.calculateStatistics()
        self.findCorrelation()
        self.calculateMeanSignal(normalized=False)

        self.printStableAps()
        #self.plotSampleSizeDistribution()
        self.printAPsSortedBySignalStrenght()
        #self.plotStats(0,includeMeanSignal=True)
        #self.plotSignal(3,showMeanSignal=False,showExtraData=False)
        #self.printCorrelation()
        #self.plotStdToSignalLevel()

    def plotSampleSizeDistribution(self):
        stable = list()
        for network in self.networks:
            stable.append(len(network.samples)/float(self.numOfSamples))
        pl.hist(stable)
        pl.xlabel("SSID occurrence (in percent)")
        pl.ylabel("Num of different SSIDs")
        pl.show()


    @staticmethod
    def getMeanDistances(wifi1,wifi2):
        distances = [ ]
        for network in wifi1.getAPsSortedBySignalStrenght()[:7]:
            network2 = wifi2.getNetworkWithMAC(network.mac)
            distance = abs(network2.mean - network.mean)
            distances.append(distance)
        return np.mean(distances), np.max(distances)




    def getMeanStdFromAllStableSignals(self):
        stds = [ ]
        vars = [ ]
        for network in self.getAPsSortedBySignalStrenght():
            stds.append(network.std)
            vars.append(network.var)
        return np.mean(stds), np.std(stds)

    @staticmethod
    def plotDistancesForMeanSignal(firstBlack = False):
        i = 0
        if firstBlack:
            i = -1
        for wifiStats in WifiStats.listOfInstances:
            signal = sorted(wifiStats.meanSignal)
            fit = stats.norm.pdf(signal,wifiStats.mean, wifiStats.std)
            if i == -1:
                pl.plot(signal,fit,'-o',color='black')
                i = 0
            else:
                pl.plot(signal,fit,'-o',color=WifiStats.colors[i])
            i = i + 1
            if i >= len(WifiStats.colors):
                i = 0
        plt.show()

    @staticmethod
    def plotDistancesForMac(mac, showMeanDiff = False,firstBlack = False):
        listOfNetworks = list()
        for wifiStats in WifiStats.listOfInstances:
            listOfNetworks.append(wifiStats.getNetworkWithMAC(mac))
        i = 0
        if firstBlack:
            i = -2
        for network in listOfNetworks:
            signal = network.getSortedSignal()
            fit = stats.norm.pdf(signal,network.mean, network.std)
            if i < 0:
                pl.plot(signal,fit,'-o',color='black')
            else:
                pl.plot(signal,fit,'-o',color=WifiStats.colors[i])
            #pl.hist(signal,normed=True,color=colors[i])
            i = i + 1
            if i >= len(WifiStats.colors):
                i = 0
        pl.show()

    def calculateMeanSignal(self, normalized = False):
        if len(self.getStableAps()) > 1:
            allSignals = list()
            minSignalLen = sys.maxint
            for network in self.getStableAps():
                if not normalized:
                    allSignals.append(network.getSignal())
                else:
                    signalRaw = network.getSignal()
                    signalRaw = signalRaw / np.linalg.norm(signalRaw)
                    signalRaw = 3000*np.array(signalRaw)
                    allSignals.append(signalRaw)
                if len(network.getSignal()) < minSignalLen:
                    minSignalLen = len(network.getSignal())
            sumSignal = list()
            for i in range(0,minSignalLen):
                sumSignal.append(0)
            for i in range(0,minSignalLen):
                    for signal in allSignals:
                            sumSignal[i] = sumSignal[i] + signal[i]
            for i in range(0,len(sumSignal)):
                sumSignal[i] = sumSignal[i] / float(len(allSignals))

            self.meanSignal = sumSignal
            self.mean = np.mean(self.meanSignal)
            self.std = np.std(self.meanSignal)
        #pl.plot(sumSignal)
        #pl.show()


    def plotStdToSignalLevel(self):
        averageSignal = [ ]
        std = []
        var = []
        for network in self.getStableAps():
            averageSignal.append(network.mean)
            std.append(network.std)
            #var.append(network.var)

        # calculate polynomial
        z = np.polyfit(averageSignal, std, 2)
        f = np.poly1d(z)

        # calculate new x's and y's
        x_new = np.linspace(averageSignal[0], averageSignal[-1], 50)
        y_new = f(x_new)

        plt.plot(averageSignal,std,'o', x_new, y_new)
        #plt.xlim([averageSignal[0]-1, averageSignal[-1] + 1 ])
        plt.show()
        #plt.scatter(averageSignal,std,color='red')
        #plt.scatter(averageSignal,var,color='red')
        #plt.show()

    def corelationBetweenSignals(self,numOfAps,numOfSamples):
        correlations = []
        for network in self.getAPsSortedBySignalStrenght()[:numOfAps]:
            for network2 in self.getAPsSortedBySignalStrenght()[:numOfAps]:
                if network.mac != network2.mac:

                    # comparing data length
                    lenData1 = len(network.groupBySamples(numOfSamples))
                    lenData2 = len(network2.groupBySamples(numOfSamples))
                    length = 0
                    if lenData1 > lenData2:
                        length = lenData2
                    else:
                        length = lenData1

                    # calculating correlations
                    correlation = stats.pearsonr(network.groupBySamples(numOfSamples)[:length], network2.groupBySamples(numOfSamples)[:length])
                    correlations.append(correlation)

        for c in correlations:
            print WifiStats.pValueToSignificance(c[1])
            #print c
        print "Mean: "+WifiStats.pValueToSignificance(np.mean(correlations[:1]))

    def findCorrelation(self):
        for network in self.networks:
            network.findCorelation()

    def printCorrelation(self):
        print "Temp:"
        for network in self.getAPsSortedBySignalStrenght():
            print str(network.ssid)+":"+str(network.tempCorrelation)+":"+WifiStats.pValueToSignificance(network.tempCorrelation[1])
        print "Motion:"
        for network in self.getAPsSortedBySignalStrenght():
            print str(network.ssid)+":"+str(network.motionCorrelation)+":"+WifiStats.pValueToSignificance(network.motionCorrelation[1])
        print "Sun:"
        for network in self.getAPsSortedBySignalStrenght():
            print str(network.ssid)+":"+str(network.sunCorrelation)+":"+WifiStats.pValueToSignificance(network.sunCorrelation[1])

    @staticmethod
    def pValueToSignificance(p):
        if(p < 0.001):
            return "highly significant"
        if(p < 0.05):
             return "significant"
        return "non-significant"


    def plotSignalGrouped(self,numOfSignals,samplesInGroup):
        for network in self.getAPsSortedBySignalStrenght()[:numOfSignals]:
            pl.plot(network.groupBySamples(samplesInGroup))
        pl.show()

    def plotSignal(self,numOfSignals,showMeanSignal = False,showExtraData = False):
        for network in self.getAPsSortedBySignalStrenght()[:numOfSignals]:
            pl.plot(network.getSignal())
        if showMeanSignal:
            pl.plot(self.meanSignal,color='black')
        if showExtraData:
            # sun
            sunData = self.getAPsSortedBySignalStrenght()[0].getSun()
            norm1izedSunData = sunData / np.linalg.norm(sunData)
            norm1izedSunData = 500*np.array(norm1izedSunData)
            pl.plot(norm1izedSunData,color='yellow')
            # motion
            motionData = self.getAPsSortedBySignalStrenght()[0].getMotion()
            norm1izedMotionData = motionData / np.linalg.norm(motionData)
            norm1izedMotionData = 100*np.array(norm1izedMotionData)
            pl.plot(norm1izedMotionData,color='green')
            # temp
            tempData = self.getAPsSortedBySignalStrenght()[0].getTemp()
            norm1izedTempData = tempData / np.linalg.norm(tempData)
            norm1izedTempData = 2000*np.array(norm1izedTempData)
            pl.plot(norm1izedTempData,color='blue')
        pl.xlabel("Time")
        pl.ylabel("Signal level")
        pl.show()

    def plotStats(self,numOfSignals,includeMeanSignal = False):
        list = self.getAPsSortedBySignalStrenght()
        for network in list[:numOfSignals]:
            signal = network.getSortedSignal()
            fit = stats.norm.pdf(signal,network.mean, network.std)
            pl.plot(signal,fit,'-o')
            #pl.hist(signal,normed=True)
        if includeMeanSignal:
            signal = sorted(self.meanSignal)
            print len(self.meanSignal)
            fit = stats.norm.pdf(signal,self.mean, self.std)
            pl.plot(signal,fit,'-o')
        pl.show()

    def calculateStatistics(self):
        for network in self.networks:
            network.computeStatistics()

        sumStd = 0
        sumMean = 0
        sumVar= 0
        for network in self.networks:
            sumStd = sumStd + network.std
            sumMean = sumMean + network.mean
            sumVar = sumVar + network.var


    def calculateAverageSignal(self):
        for network in self.networks:
            network.computeAverageSignal()


    def getAPsSortedBySignalStrenght(self):
        list = self.getStableAps()
        newlist = sorted(list, key=lambda x: x.averageSignal, reverse=True)
        return newlist

    def printAPsSortedBySignalStrenght(self,limit = 100):
        list = self.getAPsSortedBySignalStrenght()
        i = 0
        for item in list:
            print str(item.ssid)+":"+str(item.mac)+":"+str(item.averageSignal)
            i = i +1
            if i == limit:
                break

    def getNetworkWithMAC(self,mac):
        for network in self.networks:
            if mac.lower() == network.mac:
                return network
        raise Exception("Not found!")




    def getStableAps(self):
        stable = list()
        for network in self.networks:
            if network.stable:
                stable.append(network)
        return stable

    def printStableAps(self):
#        for network in self.networks:
#            if network.stable:
#                print network.mac
        print "Stable: "+str(len(self.getStableAps()))
        print "All: "+str(len(self.networks))


    def determineStableAp(self,percent_stable = 0.80):
        for network in self.networks:
            if len(network.samples) >= percent_stable*self.numOfSamples:
                network.stable = True


    def loadFromFile(self,extended = True, debug = False):
        line = 0
        macs = set()
        f = open(self.filename, 'rb')
        csvFile = csv.reader(f, delimiter=';', quotechar='"')
        last_timestamp = 0
        for row in csvFile:
                line = line + 1
                sun = 0
                motion = 0
                temp = 0
                timestamp = row[0]
                if last_timestamp != timestamp:
                    self.numOfSamples = self.numOfSamples + 1
                    last_timestamp = timestamp
                ssid = str(row[1])
                mac = str(row[2])
                if ssid == "" or ssid==" ":
                    print "Error during reading on line: "+str(line)
                    print row
                    raise Exception("Error during reading")
                if mac == "" or mac == " ":
                    print "Error during reading on line: "+str(line)
                    print row
                    raise Exception("Error during reading")
                try:
                    if extended:
                        quality = int(row[3])
                        level = int(row[4])
                    else:
                        level = int(row[3])
                        quality = 0
                except:
                    print "Error during reading on line: "+str(line)
                    print row
                    raise Exception("Error during reading")
                if extended:
                    try:
                        sun = int(row[5])
                    except:
                        if debug:
                            print "Sun error on line: "+str(line)
                            print row
                        sun = 1
                    try:
                        motion = int(row[6])
                    except:
                        if debug:
                            print "Motion error on line: "+str(line)
                            print row
                        motion = 1
                    try:
                        temp = float(row[7])
                    except:
                        if debug:
                            print "Temp error on line: "+str(line)
                            print row
                        temp = 1
                if ssid not in self.excludedSSIDs:
                    if mac not in macs:
                        self.networks.append(NetworkHolder(ssid,mac))
                        macs.add(mac)
                    network = self.getNetworkWithMAC(mac)
                    if not extended:
                        network.addSample(timestamp,level,quality)
                    else:
                        network.addSample(timestamp,level,quality,sun,motion,temp)

