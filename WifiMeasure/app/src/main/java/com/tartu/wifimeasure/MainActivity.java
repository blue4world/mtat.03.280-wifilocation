package com.tartu.wifimeasure;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    TextView tx;
    TextView txNumOfScans;
    WifiManager wifi;
    String wifis[];
    WifiScanReceiver wifiReciever;
    Boolean scanning = false;
    PowerManager.WakeLock wakeLock = null;
    FileOutputStream f;
    PrintWriter pw;
    int numOfScans = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanning == false) {
                    Snackbar.make(view, "Wifi scan started", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    scanning = true;
                    tx.setText("Scanning...");
                    wifiReciever = new WifiScanReceiver();
                    registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                    openFile();
                    enableWakeLock();
                    lv.setVisibility(View.VISIBLE);

                }
                else {
                    Snackbar.make(view, "Wifi scan stoped", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    scanning = false;
                    closeFile();
                    unregisterReceiver(wifiReciever);
                    disableWakeLock();
                    tx.setText("Not scanning");
                    lv.setVisibility(View.INVISIBLE);
                    numOfScans = 0;
                }
            }
        });

        lv = (ListView) findViewById(R.id.listView);
        tx = (TextView) findViewById(R.id.text);
        txNumOfScans = (TextView) findViewById(R.id.textNumOfScans);
        tx.setText("Not scanning");
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        Timer tim = new Timer();
        tim.schedule(new TimerTask(){
            @Override
            public void run(){
                wifi.startScan();
            }
        }, 0, 100);
        //wifi.startScan();


    }

    public void enableWakeLock(){
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakelockTag");
        wakeLock.acquire();
    }

    public void disableWakeLock(){
        if(wakeLock != null ) {
            if(wakeLock.isHeld()) {
                wakeLock.release();
            }
        }
    }

    @Override
    protected void onStop() {
        closeFile();
        super.onStop();
    }

    @Override
    protected void onPause() {
        //closeFile();
        try {
            unregisterReceiver(wifiReciever);
        }
        catch (Exception e){

        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        //openFile();
        registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class WifiScanReceiver extends BroadcastReceiver{
        public void onReceive(Context c, Intent intent) {
            numOfScans = numOfScans + 1;
            List<ScanResult> wifiScanList = wifi.getScanResults();
            wifis = new String[wifiScanList.size()];

            for(int i = 0; i < wifiScanList.size(); i++){
                wifis[i] = ((wifiScanList.get(i)).toString());
                //pw.write((wifiScanList.get(i)).toString());
                long time = System.currentTimeMillis();
                pw.write(Long.toString(time));
                pw.write(";");
                pw.write((wifiScanList.get(i)).SSID);
                pw.write(";");
                pw.write((wifiScanList.get(i)).BSSID);
                pw.write(";");
                pw.write(Integer.toString((wifiScanList.get(i)).level));
                pw.write("\n");

            }
            if(numOfScans % 10 == 0) {
                lv.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, wifis));
            }
            txNumOfScans.setText(Integer.toString(numOfScans));
        }
    }


    public void openFile(){
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File (root.getAbsolutePath() + "/wifiLocation");
        dir.mkdirs();
        long time = System.currentTimeMillis();
        File file = new File(dir, Long.toString(time)+".txt");
        try {
            f = new FileOutputStream(file);
            pw = new PrintWriter(f);
        }
        catch (Exception e){

        }

    }

    public void closeFile(){
        try {
            pw.flush();
            pw.close();
            f.close();
        }
        catch (Exception e){

        }
    }

}
