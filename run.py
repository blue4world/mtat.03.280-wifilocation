#!/usr/bin/python
from basic_stats import *
from wifiStats import *
from stdOverTime import *

#basicStats = BasicStats("data/long_term/data_1.txt")
#basicStats.plotStableSignals(9)

#wifiStats1 = WifiStats("data/long_term/data_1.txt",True,stableAps=0.9)
#wifiStats2 = WifiStats("data/long_term/data_3.txt",True,stableAps=0.9)
#wifiStats2 = WifiStats("data/long_term/data_2.txt",True,stableAps=0.9)
#wifiStats1.plotSampleSizeDistribution()

#wifiStats1.plotSignal(5,showMeanSignal=False,showExtraData=False)
#print wifiStats1.getMeanStdFromAllStableSignals()
#stdOverTime = StdOverTime(wifiStats1,wifiStats2)
#stdOverTime.signalOverTime()

#wifiStats1.plotDistancesForMeanSignal()
#wifiStats1.plotDistancesForMac("94:b4:0f:3a:8d:c0")
#exit()

#wifiStats = WifiStats("data/long_term/short_term.txt",False,stableAps=0.1)
#WifiStats.plotDistancesForMac("94:b4:0f:3a:8d:c0")

#WifiStats.plotDistancesForMeanSignal()
#wifiStats = WifiStats("data/library_table/data_1.txt",False,stableAps=0.8)
#wifiStats.plotDistancesForMeanSignal()

#wifiStats = WifiStats("data/library_table/data_1.txt",False)
#wifiStats.plotDistancesForMeanSignal()
#wifiStats = WifiStats("data/library_table/data_3.txt",False)
#WifiStats.plotDistancesForMac("f4:4e:05:79:c8:21")


#wifiStats = WifiStats("data/library_line/data_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/library_line/data_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/library_line/data_3.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/library_line/data_4.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/library_line/data_5.txt",False,stableAps=0.1)
#WifiStats.plotDistancesForMeanSignal()
#WifiStats.plotDistancesForMac("f4:4e:05:79:c8:20")
#WifiStats.plotDistancesForMac("f4:4e:05:54:8b:80")
#WifiStats.plotDistancesForMac("f4:4e:05:0d:e7:80")

#wifiStats = WifiStats("data/corridor_1m/data_1_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_1_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_1_3.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_2_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_2_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_2_3.txt",False,stableAps=0.1)
#WifiStats.plotDistancesForMeanSignal()
#WifiStats.plotDistancesForMac("04:bd:88:b7:5d:e0",showMeanDiff=True)
#WifiStats.plotDistancesForMac("94:b4:0f:3a:8d:c0",showMeanDiff=True,firstBlack=True)

#wifiStats = WifiStats("data/corridor_05m/data_1_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_05m/data_1_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_05m/data_1_3.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_05m/data_2_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_05m/data_2_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_05m/data_2_3.txt",False,stableAps=0.1)
#WifiStats.plotDistancesForMeanSignal()
#WifiStats.plotDistancesForMac("04:bd:88:b7:5d:e0",showMeanDiff=True)
#WifiStats.plotDistancesForMac("94:b4:0f:3a:8d:c0",showMeanDiff=True,firstBlack=True)

#wifiStats = WifiStats("data/sample_rate/data_1.txt",False,stableAps=0.1) # 10
#wifiStats = WifiStats("data/sample_rate/data_2.txt",False,stableAps=0.1) # 25
#wifiStats = WifiStats("data/sample_rate/data_3.txt",False,stableAps=0.1) # 50
#WifiStats.plotDistancesForMac("94:b4:0f:3a:8d:c0",showMeanDiff=True)


#wifiStats = WifiStats("data/long_term/data_1.txt",False,stableAps=0.8)
#wifiStats.corelationBetweenSignals()
#wifiStats.plotSignalGrouped(5,100)
#wifiStats.plotSignal(3,showMeanSignal=False)

#wifiStats = WifiStats("data/one_spot/data_1.txt",False,stableAps=0.01)
#wifiStats.corelationBetweenSignals(5,20)
#wifiStats.plotSignalGrouped(3,5)

#wifiStats1 = WifiStats("data/long_term/data_1.txt",False,stableAps=0.8)
#wifiStats2 = WifiStats("data/long_term/data_1.txt",False,stableAps=0.8)
#std = StdOverTime(wifiStats2,wifiStats2)
#std.plotStdOverTime()

#wifiStats1 = WifiStats("data/corridor_05m/data_2_1.txt",False,stableAps=0.1)
#wifiStats2 = WifiStats("data/corridor_05m/data_2_2.txt",False,stableAps=0.1)
#wifiStats3 = WifiStats("data/corridor_05m/data_2_3.txt",False,stableAps=0.1)
#print WifiStats.getMeanDistances(wifiStats2,wifiStats3)

#wifiStats = WifiStats("data/corridor_1m/data_2_1.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_2_2.txt",False,stableAps=0.1)
#wifiStats = WifiStats("data/corridor_1m/data_2_3.txt",False,stableAps=0.1)

