from scipy.stats import ks_2samp
from scipy.stats import mannwhitneyu
import numpy as np
from wifiStats import *

class CompareDistributions:

    @staticmethod
    def alterSignal(signal,value):
        newSignal = [0] * len(signal)
        for i in range(0,len(signal)):
            newSignal[i] = signal[i] + value
        return newSignal

    @staticmethod
    def doKSinWithShift(signal1,signal2,shift):
        results_KS = [ ]
        results_P = [ ]

        for i in range(-shift,shift):
            signal1_mod = CompareDistributions.alterSignal(signal1,i)
            tmp_ks, tmp_ks_p_value = ks_2samp(signal1_mod, signal2)
            results_KS.append(tmp_ks)
            results_P.append(tmp_ks_p_value)

        return np.min(results_KS), np.min(results_P)



    @staticmethod
    def test():
        x = np.random.normal(0,1,6000)
        y = np.random.normal(0,1,6000)
        print ks_2samp(x, y)

    @staticmethod
    def extractValue(array,position):
        results = [ ]
        for i in range(0,len(array)):
            ks,p_value = array[i]
            if position == 0:
                results.append(ks)
            else:
                results.append(p_value)
        return results

    @staticmethod
    def run(sample):
        dataKS = dict()
        dataKS_P = dict()
        dataKS_shift = dict()
        dataKS_P_shift = dict()
        dataMW = dict()
        dataMW_P = dict()
        for wifiStats in WifiStats.listOfInstances:
            if "central_spot" not in wifiStats.filename:
                ks_results = [ ]
                ks_p_results = [ ]
                ks_shift_results = [ ]
                ks_shift_p_results = [ ]
                mw_results = [ ]
                mw_p_results = [ ]
                for network in sample.getAPsSortedBySignalStrenght():
                    if "eduroam" in network.ssid:
                        try:
                            networkFromFingerPrint = wifiStats.getNetworkWithMAC(network.mac)
                            networkFromSample = network

                            ks, ks_p_value =  ks_2samp(networkFromSample.getSignal(), networkFromFingerPrint.getSignal())
                            ks_results.append(ks)
                            ks_p_results.append(ks_p_value)

                            ks_shift, ks_shift_p_value = CompareDistributions.doKSinWithShift(networkFromSample.getSignal(), networkFromFingerPrint.getSignal(),3)
                            ks_shift_results.append(ks_shift)
                            ks_shift_p_results.append(ks_shift_p_value)

                            mw, mw_p_value = mannwhitneyu(networkFromSample.getSignal(), networkFromFingerPrint.getSignal())
                            mw_results.append(mw)
                            mw_p_results.append(mw_p_value)
                        except:
                            pass

                dataKS[wifiStats.filename] = np.mean(ks_results)
                dataKS_P[wifiStats.filename] = np.mean(ks_p_results)

                dataKS_shift[wifiStats.filename] = np.mean(ks_shift_results)
                dataKS_P_shift[wifiStats.filename] = np.mean(ks_shift_p_results)

                dataMW[wifiStats.filename] = np.mean(mw_results)
                dataMW_P[wifiStats.filename] = np.mean(mw_p_results)

        print "KS"
        print sorted(dataKS.items(), key=(operator.itemgetter(1)))
        print "KS with shift"
        print sorted(dataKS_shift.items(), key=(operator.itemgetter(1)))
        print "MW"
        print sorted(dataMW.items(), key=(operator.itemgetter(1)), reverse=True)


        #print "P-value"
        #print sorted(dataP.items(), key=(operator.itemgetter(1)),reverse=True)