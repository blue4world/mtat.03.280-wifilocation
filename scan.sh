#!/bin/bash
while true
do 
sudo iwlist wlan0 scan | grep -A 5 "Address:" | sed -e 's/--/#/g' > /tmp/scan
last=$(cat /tmp/scan | grep Cell | tail -n 1 | cut -d "-" -f 1-1 | cut -d "l" -f 2- | cut -c 2- | tr -d " ")
if [ ! -z "$last" -a "$last" != " " ]; then
echo "Number of captured APs $last"
time=$(date +%s)
for i in $(seq 1 $last)
do
if [ "$i" -lt "10" ]; then
i="0$i"
fi
cat /tmp/scan | grep -A 5 "Cell $i" > /tmp/scan_item
mac=$(cat  /tmp/scan_item | head -n 1 | cut -d ":" -f 2- | tr -d " ")
quality=$(cat /tmp/scan_item | head -n 4 | tail -n 1 | cut -d "=" -f 2-2 | cut -d "/" -f 1-1)
signal_level=$(cat /tmp/scan_item | head -n 4 | tail -n 1 | cut -d "=" -f 3-3 | cut -d " " -f  1-1)
ssid=$(cat /tmp/scan_item | tail -n 1 | cut -d '"' -f 2-2)
sun_shine=$(cat /tmp/sunshine)
motion=$(cat /tmp/motion)
temp=$(cat /tmp/temp)
echo "$time;$ssid;$mac;$quality;$signal_level;$sun_shine;$motion;$temp" >> data.txt
done
else
echo "No data"
fi
done

