#!/usr/bin/python
import csv
import matplotlib.pyplot as plt

macs = set()
ssids = set()
samples = set()
averageSignal = dict()
signalLevel = dict()
f = open('original_data/data_1.txt', 'rb')
csv = csv.reader(f, delimiter=';', quotechar='"')

plt.show()

for row in csv:
        samples.add(row[0])
        macs.add(row[2])
        ssids.add(row[1])
#print "Unique SSID: "+str(len(ssids))
#print ssids
#print "Unique MACS: "+str(len(macs))
#print macs
#print "Num of samples: "+str(len(samples))


for mac in macs:
        averageSignal[mac] = 0
        signalLevel[mac] = list()

f.seek(0)
for row in csv:
        averageSignal[row[2]] += int(row[4])
        averageSignal[row[2]] = averageSignal[row[2]] / 2
        signalLevel[row[2]].append(row[4])

#print "Average signal:"
#print averageSignal


