from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes
import numpy as np
import matplotlib.pyplot as plt


class StdOverTime:
    def __init__(self,wifiStats1,wifiStats2):
        self.wifiStats1 = wifiStats1
        self.wifiStats2 = wifiStats2

    def signalOverTime(self):
        means1 = []
        means2 = []
        for hour in range(1,5):
            error = False
            samplesForHours = []
            for network in self.wifiStats1.networks:
                if network.stable == True:
                    try:
                        samples = network.getSamplesForSpecificHour(hour)
                        for sample in samples:
                            samplesForHours.append(sample)
                    except:
                        print "Error"
                        error = True
            means1.append(int(np.average(samplesForHours)))

        for hour in range(1,5):
            error = False
            samplesForHours = []
            for network in self.wifiStats2.networks:
                if network.stable == True:
                    try:
                        samples = network.getSamplesForSpecificHour(hour)
                        for sample in samples:
                            samplesForHours.append(sample)
                    except:
                        print "Error"
                        error = True
            means2.append(int(np.average(samplesForHours)))


        print means1
        print means2
        plt.plot(means1)
        plt.plot(means2)
        ax = plt.axes()
        ylim(27,33)
        xlim(0,6)
        ax.set_xticklabels(['0-6','6-12','12-18','18-24'])
        plt.show()


    def plotStdOverTime(self):
        # Some fake data to plot
        #A = [1, 2, 5]
        #B = [2, 3, 4]

        fig = figure()
        ax = axes()
        hold(True)

        # first boxplot pair
        #bp = boxplot(A, positions = [1], widths = 0.6)
        #setBoxColors(bp)

        i = 1
        allMeans = [ ]
        for hour in range(1,5):
            means = [ ]
            error = False

            for network in self.wifiStats1.networks:
                if network.stable == True:
                    try:
                        samples = network.getSamplesForSpecificHour(hour)
                        mean = np.var(samples)
                        means.append(int(mean))
                        allMeans.append(int(mean))
                    except:
                        print "Error"
                        error = True

            for network in self.wifiStats2.networks:
                if network.stable == True:
                    try:
                        samples = network.getSamplesForSpecificHour(hour)
                        mean = np.var(samples)
                        means.append(int(mean))
                        allMeans.append(int(mean))
                    except:
                        print "Error"
                        error = True


            if error == False:
                bp = boxplot(means, positions = [i], widths = 0.6)

            i = i + 1

        bp = boxplot(allMeans, positions = [5], widths = 0.6)
        # set axes limits and labels
        xlim(0,6)
        ylim(0,25)
        ax.set_xticklabels(['0-6','6-12','12-18','18-24','All'])
        ax.set_xticks(range(1,6))

        show()



"""
    def plot(self):
        # Create a figure instance
        fig = plt.figure(1, figsize=(9, 6))

        # Create an axes instance
        ax = fig.add_subplot(111)


        dicHors = dict()
        for hour in range(0,23):
            dicHors[hour] = []
            for network in self.wifiStats1.networks:
                if network.stable == True:
                    samples = network.getSamplesForSpecificHour(hour)
                    dicHors[hour].append(np.std(samples))

        # Create the boxplot
        dataToPlot = []
        for hour in dicHors.keys():
            dataToPlot.append(dicHors[hour])
        bp = ax.boxplot(dataToPlot)

        fig.show()
"""
'''
    def plot(self):
        dicHors = dict()

        for hour in range(0,23):
            dicHors[hour] = []
            for network in self.wifiStats1.networks:
                if network.stable == True:
                    samples = network.getSamplesForSpecificHour(hour)
                    dicHors[hour].append(np.std(samples))
            #print dicHors[hour]

        fig = figure()
        ax = axes()
        hold(True)
        i = 1
        for hour in range(0,23):
            boxplot(dicHors[hour],widths = 0.6)
            i += 1

        #for hour in range(0,23):
        #    ax.set_xticklabels(str(hour))

        # draw temporary red and blue lines and use them to create a legend
        #hB, = plot([1,1],'b-')
        #hR, = plot([1,1],'r-')
        #legend((hB, hR),('Dataset 1', 'Dataset 2'))
        #hB.set_visible(False)
        #hR.set_visible(False)
        show()

        # Some fake data to plot
        A= [[1, 2, 5,],  [7, 2]]
        B = [[5, 7, 2, 2, 5], [7, 2, 5]]
        C = [[3,2,5,7], [6, 7, 3]]

        fig = figure()
        ax = axes()
        hold(True)

        # first boxplot pair
        bp = boxplot(A, positions = [1, 2], widths = 0.6)
        #setBoxColors(bp)

        # second boxplot pair
        bp = boxplot(B, positions = [4, 5], widths = 0.6)
        #setBoxColors(bp)

        # thrid boxplot pair
        bp = boxplot(C, positions = [7, 8], widths = 0.6)
        #setBoxColors(bp)

        # set axes limits and labels
        xlim(0,9)
        ylim(0,9)
        ax.set_xticklabels(['A', 'B', 'C'])
        ax.set_xticks([1.5, 4.5, 7.5])

        # draw temporary red and blue lines and use them to create a legend
        hB, = plot([1,1],'b-')
        hR, = plot([1,1],'r-')
        legend((hB, hR),('Dataset 1', 'Dataset 2'))
        hB.set_visible(False)
        hR.set_visible(False)

        #savefig('boxcompare.png')
        show()

                    '''