import datetime
class SampleHolder:
    def __init__(self,time,signal_level,signal_quality,sun = 0,motion = 0,temperature = 0):
        #self.time = datetime.fromtimestamp(time)
        self.timestamp = time
        self.level = signal_level
        self.quality = signal_quality
        self.motion = motion
        self.sun = sun
        self.temperature = temperature
