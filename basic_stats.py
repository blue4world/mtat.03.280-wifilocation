import csv
import operator
import matplotlib.pyplot as plt


class BasicStats:
        def __init__(self, filename):
                self.macs = set()
                self.ssids = set()
                self.samples = set()
                self.averageSignal = dict()
                self.occurrence = dict()
                self.filename = filename
                self.stableAPs = list()
                self.excludedAps = ["94:B4:0F:3A:8D:C1", "04:BD:88:B7:5D:E0"]

                # run
                self.computeStats()
                #self.plotStableSignals()
                #self.plotHistogram()



        def  plotStableSignals(self,numOfAps):
                signalLevels = dict()
                for mac in self.stableAPs:
                        signalLevels[mac] = list()
                f = open(self.filename, 'rb')
                csvFile = csv.reader(f, delimiter=';', quotechar='"')
                for row in csvFile:
                        if row[2] in self.stableAPs:
                                signalLevels[row[2]].append(100-abs(int(row[4])))
                
                i=0
                for mac in sorted(self.averageSignal, key=self.averageSignal.get, reverse=True):
                        if mac in self.stableAPs:
                                if mac not in self.excludedAps:
                                        print "Ploting: "+mac
                                        plt.plot(signalLevels[mac])
                                        i=i+1
                                        if i >= numOfAps:
                                                break
                plt.show()
                        


        def computeStats(self):
                f = open(self.filename, 'rb')
                csvFile = csv.reader(f, delimiter=';', quotechar='"')
                for row in csvFile:
                        self.samples.add(row[0])
                        self.macs.add(row[2])
                        self.ssids.add(row[1])        

                for mac in self.macs:
                        self.averageSignal[mac] = 0
                        self.occurrence[mac] = 0
                f.seek(0)
	        for row in csvFile:
                        self.occurrence[row[2]] +=1
                        self.averageSignal[row[2]] += 100-abs(int(row[4]))
                        self.averageSignal[row[2]] = self.averageSignal[row[2]] / 2
                self.stableAPs = self.getOnlyStableAp()

        def getOnlyStableAp(self):
                samplesSize = len(self.samples)
                stableAps = list()
                for mac in self.macs:
                        if(self.occurrence[mac] >= samplesSize*0.9):
                                stableAps.append(mac)
                return stableAps
