from wifiStats import *
from scipy.spatial import distance
import math
import operator

class Positioning:
    def __init__(self):
        self.distancesEuclidean = dict()
        self.distancesEuclideanShift = dict()


    def calculareManhattan(self,sample):
        for wifiStats in WifiStats.listOfInstances:
            if "central_spot" not in wifiStats.filename:
                distance = 0
                for network in sample.getAPsSortedBySignalStrenght():
                    if "eduroam" in network.ssid:
                        try:
                            networkFromFingerPrint = wifiStats.getNetworkWithMAC(network.mac)
                            distance += abs(network.mean - networkFromFingerPrint.mean)
                        except:
                            pass
                self.distancesEuclidean[wifiStats.filename] = distance

        print sorted(self.distancesEuclidean.items(), key=operator.itemgetter(0))


    def calculareStandartEuclidean(self,sample):
        for wifiStats in WifiStats.listOfInstances:
            if "central_spot" not in wifiStats.filename:
                distance = 0
                for network in sample.getAPsSortedBySignalStrenght():
                    if "eduroam" in network.ssid:
                        try:
                            networkFromFingerPrint = wifiStats.getNetworkWithMAC(network.mac)
                            distance += (network.mean - networkFromFingerPrint.mean)**2
                        except:
                            pass
                self.distancesEuclidean[wifiStats.filename] = math.sqrt(distance)

        print sorted(self.distancesEuclidean.items(), key=operator.itemgetter(1))


    def calculateEuclidianWithShift(self,sample,shiftVal):
        for wifiStats in WifiStats.listOfInstances:
            if "central_spot" not in wifiStats.filename:
                distances = [ ]
                for shift in range(-shiftVal,shiftVal):
                    distance = 0
                    for network in sample.getAPsSortedBySignalStrenght():
                        if "eduroam" in network.ssid:
                            try:
                                networkFromFingerPrint = wifiStats.getNetworkWithMAC(network.mac)
                                distance += (network.mean - (networkFromFingerPrint.mean+shift))**2
                            except:
                                pass
                    distances.append(math.sqrt(distance))
                self.distancesEuclideanShift[wifiStats.filename] = np.min(distances)

        print sorted(self.distancesEuclideanShift.items(), key=operator.itemgetter(1))







